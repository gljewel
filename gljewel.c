/**
 * @file gljewel.c
 * @brief The main source file for gljewel.
 *
 * Copyright 2001, 2008 David Ashley  <dashxdr@gmail.com>
 * Copyright 2008 Stephen M. Webb  <stephen.webb@bregmasoft.ca>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of Version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if defined(_WIN32) && !defined(__CYGWIN__)
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif
#include <fcntl.h>
#include <stdlib.h>

#include <stdarg.h>
#include <GL/gl.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <signal.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>
#include <SDL/SDL.h>
#include <SDL/SDL_main.h>

#include "bcube.h"
#include "bucky.h"
#include "cylinder.h"
#include "diamond.h"
#include "ico.h"
#include "misc.h"
#include "matrix.h"
#include "pyramid.h"
#include "shape.h"
#include "sphere.h"
#include "text.h"

typedef unsigned int uint32;

#define SWAPTIME 20

#define HIGHSCOREFILENAME ".gljewelscores"

enum sounds {
ROWSOUND,
DROPSOUND,
BIGDROPSOUND,
SWAPSOUND,
GAMEOVERSOUND,
ALERTSOUND,
ILLEGALSOUND,
};

#define BDIST 1000.0f

shape shapes[9];
static GLint objectlists;

char clearing=1,clearonce=0;

float life,decay;
int stage,score,level;
int nummoves;
int gamestate;
int cumulative;
int cumulativebuildup;
float cumulativefade;
int enteringline;
int baseshow;

#define STARTLIFE 1000.0f
#define ALARMLEVEL 10.0f // seconds before death
#define CREDIT 50.0f
#define INITIALDECAY 0.4f
#define DECAYADD 0.02f
#define NEXTLEVEL 10

#define LIFEVISIBLE (STARTLIFE*2.0f)

// position/scaling settings
#define SCALE 0.88f // size of each jewel
#define SPACING 1.76f // how much to move for each row/column
#define SHIFTX 3.0f // shift the jewels sideways so they're not centered
#define SHIFTY 0.0f

// text settings
#define SCOREX -8.0f // coords for upper left
#define SCOREY 5.0f
#define SCOREZ 8.0f
#define FONTSIZE 0.015f
#define LINESPACE (FONTSIZE*70)
#define SCORETAB 4.3f // move to right for even spacing

#define HIGHX -2.0f
#define HIGHY 4.2f
#define HIGHZ 8.0f
#define HIGHXNUMBER 0.0f
#define HIGHXNAME 1.3f
#define HIGHXSCORE 5.0f
#define HIGHXLEVEL 2.5f
#define HIGHYSPACE (-FONTSIZE*60)

#define HIGHLEFT -3.5f
#define HIGHRIGHT 8.2f
#define HIGHTOP 5.7f
#define HIGHBOTTOM (-HIGHTOP)

#define MAXHIGHSCORES 100

#define MAXNAMELENGTH 12
struct highscore {
char name[MAXNAMELENGTH];
int score;
int level;
} highscores[MAXHIGHSCORES];

char lastentered[MAXNAMELENGTH]={0};

enum gamestate {
GAMEOVER,
PLAYING,
ENTERHIGH,
};

static GLfloat lightpos[4] = {-2.0f, 4.0f, 4.0f, 0.0f};
static GLfloat light1pos[4] = {22.0f, 2.0f, 4.0f, 0.0f};
static GLfloat light2pos[4] = {0.0f, 0.0f, 4.0f, 0.0f};
static GLfloat red[4] = {0.8f, 0.1f, 0.0f, 1.0f};
static GLfloat green[4] = {0.0f, 0.8f, 0.2f, 1.0f};
static GLfloat yellow[4] = {1.0f, 1.0f, 0.0f, 1.0f};
static GLfloat blue[4] = {0.2f, 0.2f, 1.0f, 1.0f};
static GLfloat white[4] = {1.0f, 1.0f, 1.0f, 1.0f};
static GLfloat magenta[4] = {1.0f, 0.0f, 1.0f, 1.0f};
#define WH2 0.7f
static GLfloat white2[4] = {WH2, WH2, WH2, 1.0f};
static GLfloat orange[4] = {1.0f, 0.5f, 0.0f, 1.0f};
#define GV 0.75f
static GLfloat grey[4] = {GV,GV,GV,1.0f};
#define GV2 0.75f
static GLfloat grey2[4] = {GV2,GV2,GV2,1.0f};

static GLfloat *colormaps[]={blue,orange,yellow,magenta,green,red,white};
static GLfloat dimmer[4]={0.0f, 0.0f, 0.0f, 0.5f};

vector eye;

#define EX 8
#define EY 8

struct element {
int type;
float ax,ay,az;
float angle;
float fall,speed;
float vanish;
float dx,dy;
int swapping;
} elements[EY][EX];

#define MAXSCORESPRITES 12
struct scoresprite {
float x,y,z;
float fade;
int value;
} scoresprites[MAXSCORESPRITES];

int mousex,mousey,sizex,sizey;


uint32 *tpixels;
uint32 torgba(unsigned int r,unsigned int g,unsigned int b,unsigned int a)
{
	r&=255;
	g&=255;
	b&=255;
	a&=255;
	return (a<<24) | (r<<0) | (g<<8) | (b<<16);
}

int fontloaded=0;

int fontheight;

matrix unprojectmatrix;

void initunproject(void)
{
matrix projection,model;
matrix temp;

	glGetFloatv(GL_PROJECTION_MATRIX,(void *)projection);
	glGetFloatv(GL_MODELVIEW_MATRIX,(void *)model);
	matrixXmatrix(&temp,&projection,&model);
	invertmatrix(&unprojectmatrix,&temp);
}

SDL_Surface *setvideomode(int w, int h)
{
SDL_Surface *screen;
	screen = SDL_SetVideoMode(w, h, 24, SDL_OPENGL|SDL_RESIZABLE);
	if(!screen)
	{
		fprintf(stderr, "Couldn't set %dx%d GL video mode: %s\n",
			sizex, sizey, SDL_GetError());
		SDL_Quit();
		exit(2);
	}
	return screen;
}

static void resize( unsigned int width, unsigned int height )
{
float h;

	sizex=width;
	sizey=height;

	setvideomode(width, height);

	glViewport(0, 0, (GLint) width, (GLint) height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if(width<height)
	{
		h=(float)height/(float)width;
		glFrustum(-1.0, 1.0, -h, h, 5.0, 60.0);
	} else
	{
		h=(float)width/(float)height;
		glFrustum(-h,h,-1.0,1.0,5.0,60.0);
	}
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0, 0.0, -40.0);
	initunproject();
}



#if 0
static Window make_rgb_db_window( Display *dpy,
				  unsigned int width, unsigned int height )
{
int attrib[] = { GLX_RGBA,
		GLX_RED_SIZE, 1,
		GLX_GREEN_SIZE, 1,
		GLX_BLUE_SIZE, 1,
		GLX_DOUBLEBUFFER,
		GLX_DEPTH_SIZE, 1, 
		None };
int scrnum;
XSetWindowAttributes attr;
uint32 mask;
Window root;
Window win;
GLXContext ctx;
XVisualInfo *visinfo;

	scrnum = DefaultScreen( dpy );
	root = RootWindow( dpy, scrnum );

	visinfo = glXChooseVisual( dpy, scrnum, attrib );
	if (!visinfo) {
		printf("Error: couldn't get an RGB, Double-buffered visual\n");
		exit(1);
	}

	/* window attributes */
	attr.background_pixel = 0;
	attr.border_pixel = 0;
	attr.colormap = XCreateColormap( dpy, root, visinfo->visual, AllocNone);
	attr.event_mask = StructureNotifyMask | ExposureMask;
	mask = CWBackPixel | CWBorderPixel | CWColormap | CWEventMask;

	win = XCreateWindow( dpy, root, 0, 0, width, height,
				0, visinfo->depth, InputOutput,
				visinfo->visual, mask, &attr );

	ctx = glXCreateContext( dpy, visinfo, NULL, True );

	glXMakeCurrent( dpy, win, ctx );

	return win;
}
#endif


void setmaterial(GLfloat *color)
{
float shiny=25.0;
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color);
	glMaterialfv(GL_FRONT, GL_SPECULAR, white);
	glMaterialfv(GL_FRONT, GL_SHININESS, &shiny);
}

void makedots(float size)
{
int i;
float a;
#define DOTS 12
	glPointSize(3.0);
	glDisable(GL_LIGHTING);
	glBegin(GL_POINTS);
	for(i=0;i<DOTS;++i)
	{
		float u,v;
		a=i*PI*2.0f/DOTS;
		u=cosf(a)*size;
		v=sinf(a)*size;
		glVertex3f(u,v,0.0);
		glVertex3f(u,0.0,v);
	}
	glEnd();
	glEnable(GL_LIGHTING);

}

void norm2(float *p1,float *p2,float *p3)
{
float nx,ny,nz;
float x1,y1,z1;
float x2,y2,z2;

	x1=p2[0]-p1[0];
	y1=p2[1]-p1[1];
	z1=p2[2]-p1[2];

	x2=p3[0]-p1[0];
	y2=p3[1]-p1[1];
	z2=p3[2]-p1[2];

	nx=y1*z2-y2*z1;
	ny=x2*z1-x1*z2;
	nz=x1*y2-x2*y1;
	glNormal3f(nx,ny,nz);
}


void makespiky(GLfloat *color,float scale)
{
#define SPIKES 12
#define SPIKEZ 0.5f
#define SPIKEIN 0.7f
float x1[SPIKES],y1[SPIKES];
float x2[SPIKES],y2[SPIKES];
float a,b,b2;
int i,j;
float p0[3],p1[3],p2[3];

	b=PI*2.0f/SPIKES;
	b2=b/2.0f;
	for(i=0;i<SPIKES;++i)
	{
		a=i*b;
		x1[i]=cosf(a)*scale*SPIKEIN;
		y1[i]=sinf(a)*scale*SPIKEIN;
		x2[i]=cosf(a+b2)*scale;
		y2[i]=sinf(a+b2)*scale;
	}	

	glDisable(GL_TEXTURE_2D);
	glEnable(GL_NORMALIZE);
	setmaterial(color);

	glBegin(GL_TRIANGLE_FAN);
	p0[0]=0.0;
	p0[1]=0.0;
	p0[2]=SPIKEZ*scale;
	glVertex3fv(p0);
	p1[0]=x1[0];
	p1[1]=y1[0];
	p1[2]=0.0;
	glVertex3fv(p1);
	for(i=0;i<SPIKES;++i)
	{
		j=i+1;
		if(j>=SPIKES) j-=SPIKES;

		p2[0]=x2[i];
		p2[1]=y2[i];
		p2[2]=0.0;

		norm2(p0,p1,p2);
		glVertex3fv(p2);

		p1[0]=x1[j];
		p1[1]=y1[j];
		p1[2]=0.0;

		norm2(p0,p2,p1);
		glVertex3fv(p1);

	}
	glEnd();
}


float timeindicator;

void showlife(void)
{
float a,b;
int i;

#define SECTIONS 24
#define LIFEX -6.5f
#define LIFEY -1.0f
#define LIFEZ1 8.0f
#define LIFEZ2 5.0f
#define LIFESIZE 2.0f
#define LIFENORMAL 0.7f

#define SECTIONYELLOW 4
#define SECTIONRED 1


	glPushMatrix();
	glTranslatef(LIFEX,LIFEY,LIFEZ2);
	glRotatef(11.0f, 0.0f, 1.0f, 0.0f);
	b=PI*2.0f/SECTIONS;

	a=0;
	setmaterial(blue);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glBegin(GL_LINE_LOOP);
	for(i=0;i<SECTIONS;++i)
	{
		glVertex3f(sinf(a)*LIFESIZE,cosf(a)*LIFESIZE, 0.0);
		a+=b;
	}
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glEnable(GL_NORMALIZE);
	glVertex3f(0.0, 0.0, LIFEZ1-LIFEZ2);
	if(life<LIFEVISIBLE)
	{
		float x,y;
		a=PI*2.0f*life/LIFEVISIBLE;
		x=sinf(a)*LIFESIZE;
		y=cosf(a)*LIFESIZE;
		glNormal3f(x,y,LIFENORMAL);
		glVertex3f(x,y,0.0f);
		i=(int)(life*(float)SECTIONS/LIFEVISIBLE);
	} else i=SECTIONS;
	if(i>SECTIONYELLOW)
		setmaterial(green);
	else if(i>SECTIONRED)
		setmaterial(yellow);
	else setmaterial(red);

	for(;i>=0;--i)
	{
		if(i==SECTIONYELLOW) setmaterial(yellow);
		else if(i==SECTIONRED) setmaterial(red);
		a=(i-0.5f)*b;
		glNormal3f(sinf(a), cosf(a), LIFENORMAL);
		
		a=i*b;
		glVertex3f(sinf(a)*LIFESIZE,cosf(a)*LIFESIZE,0.0);
	}
	glEnd();

	glTranslatef(0.0f, -3.0f, 0.0f);
	glRotatef(timeindicator,0.0f, 0.0f, 1.0f);
	glCallList(objectlists+8);
	glPopMatrix();

}

void setgrey(float v)
{
float col[4]={v,v,v,v};
	glMaterialfv(GL_FRONT,GL_AMBIENT_AND_DIFFUSE, col);

}

void f2(float angle, GLfloat *color, int orient)
{
float nx,xt;
float ny,yt;
float nz,zt;
int i,j,k;
float t1,t2;

	xt=yt=zt=0.0f;
	nx=ny=nz=0.0f;

//	glDisable(GL_DEPTH_TEST);
	glPushMatrix();

	nz=10.0f;

	glRotatef(angle, 0.1f, 1.0f, 0.0f);

	for(k=0;k<15;++k)
	{
		i=k%3;
		j=k/3;
		if(i==1 && j>=1 && j<4) continue;

#define TSCALE 1.3f


		nx=ny=nz=0.0;
		t1=(i-1)*TSCALE;
		t2=(j-2)*TSCALE;
		switch(orient)
		{
		case 0:
			nx += t1;
			ny += t2;
			break;
		case 1:
			nx += t2;
			nz += t1;
			break;
		case 2:
			ny += t1;
			nz += t2;
			break;
		}


		glTranslatef(nx-xt,ny-yt,nz-zt);
		xt=nx;
		yt=ny;
		zt=nz;
		glPushMatrix();
//	glRotatef(e->angle,e->ax,e->ay,e->az);
		setmaterial(color);
		glCallList(objectlists+1);
		glPopMatrix();
	}

	glPopMatrix();
}

#define FDOWN 0.5
void figure(void)
{
static float angle=0.0;
	glPushMatrix();
	glTranslatef(-5.5, -3.0, 10.0);
	glScalef(FDOWN, FDOWN, FDOWN);
	f2(angle, blue, 0);
	f2(angle, red, 1);
	f2(angle, green, 2);
	angle+=1;
	glPopMatrix();
}


float cursorrot;
int cposx=0,cposy=0;

static void draw(void)
{
int i,j,k;
struct element *e;
float xt,yt,zt,x,y,t;
int c=0;
float a;
float nx,ny,nz;

	if(clearing || clearonce)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		if(clearonce) clearonce=0;
	}

	glLightfv(GL_LIGHT0, GL_POSITION, lightpos);
	glLightfv(GL_LIGHT1, GL_POSITION, light1pos);
	glLightfv(GL_LIGHT2, GL_POSITION, light2pos);


	if(gamestate==PLAYING) showlife();

	glPushMatrix();

	t=SPACING;
	y=t*(EY/2.0f-0.5f);
	xt=yt=zt=0.0;

	e=elements[0];
	k=EX*EY;
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	for(j=0;j<EY;++j)
	{
		x=-t*(EX/2.0f-0.5f);
		for(i=0;i<EX;++i)
		{

			nx=x+SHIFTX;
			ny=y;
			nz=0.0;
nz+=(1.0f-e->vanish)*50.0f;
			if(e->swapping)
			{
				float s;
				a=e->swapping*PI/2.0f/SWAPTIME;
				s=t*cosf(a);
				nx+=s*e->dx;
				ny+=s*e->dy;
				s=t*sinf(a*2.0);
				if(c++ & 1) s=-s;
				nz+=s;
			}
			ny+=e->fall*t;
			glTranslatef(nx-xt,ny-yt,nz-zt);
			xt=nx;
			yt=ny;
			zt=nz;
			if(gamestate==PLAYING && cposx==i && cposy==j)
				glEnable(GL_LIGHT2);
			if(e->swapping) {glDisable(GL_BLEND);glEnable(GL_DEPTH_TEST);}
			glPushMatrix();
			glRotatef(e->angle,e->ax,e->ay,e->az);
//			glScalef(e->vanish,e->vanish,e->vanish);
			setmaterial(colormaps[e->type]);
			if (e->type == 2)
				shapes[2].draw();
			else
				glCallList(objectlists+e->type);
			glPopMatrix();
			if(e->swapping) {glEnable(GL_BLEND);glDisable(GL_DEPTH_TEST);}
			if(gamestate==PLAYING && cposx==i && cposy==j)
				glDisable(GL_LIGHT2);
			x+=t;
			++e;
		}
		y-=t;
	}
	glEnable(GL_DEPTH_TEST);
	if(0 && cposx>=0 && cposy>=0 && cposx<EX && cposy<EY)
	{
		nx=-SPACING*(-cposx+EY/2.0f-0.5f);
		ny= SPACING*(-cposy+EY/2.0f-0.5f);
		glTranslatef(nx-xt,ny-yt,nz-zt);
		glPushMatrix();
//		glRotatef(cursorrot,.70710,-.70710,0.0);
cursorrot+=5.0f;
//		glCallList(objectlists+7);
		glPopMatrix();
		glPushMatrix();
		glRotatef(cursorrot,1.0f,0.0f,0.0f);
		glCallList(objectlists+7);
		glPopMatrix();
	}

	glPopMatrix();

#define SCORESPRITEFADE 0.01f

	for(i=0;i<MAXSCORESPRITES;++i)
	{
		GLfloat col[4];
		struct scoresprite *ss=scoresprites+i;
		if(ss->fade==0.0) continue;
		col[0]=col[1]=col[2]=ss->fade;
		col[3]=1.0f;
		glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, col);
		glPushMatrix();
		glTranslatef(ss->x,ss->y-(1.0f-ss->fade),ss->z);
		drawstring(1,"+%d",ss->value);
		glPopMatrix();
	}

	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, red);

	glPushMatrix();
	glTranslatef(SCOREX+SCORETAB,SCOREY,SCOREZ);
	drawstring(2,"%d",score);
	glTranslatef(0.0, -LINESPACE, 0.0);
	drawstring(2,"%d",level);
	glTranslatef(0.0, -LINESPACE, 0.0);
	drawstring(2,"%d",nummoves);
	glTranslatef(0.0, -LINESPACE, 0.0);
	glPopMatrix();	

	glPushMatrix();
	glTranslatef(SCOREX,SCOREY,SCOREZ);
	drawstring(0,"Score");
	glTranslatef(0.0, -LINESPACE, 0.0);
	drawstring(0,"Level");
	glTranslatef(0.0, -LINESPACE, 0.0);
	drawstring(0,"Moves");
	if(gamestate!=PLAYING)
	{
		glTranslatef(0.0f, -3.5f*LINESPACE, 0.0f);
		drawstring(0,"Game Over");
	}
	else if(cumulativefade>0.0)
	{
		setgrey(cumulativefade);
		glEnable(GL_BLEND);
		glTranslatef(SCORETAB/2.0, -LINESPACE, 0.0);
		drawstring(1,"+%d",cumulative);
		glDisable(GL_BLEND);
	}
	glPopMatrix();

	if(gamestate!=PLAYING)
	{
		struct highscore *h=highscores+baseshow;

		glEnable(GL_BLEND);
		glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, dimmer);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_LIGHT0);
		glDisable(GL_LIGHT1);
		glBegin(GL_QUADS);
		glVertex3f(HIGHLEFT,HIGHTOP,HIGHZ);
		glVertex3f(HIGHLEFT,HIGHBOTTOM,HIGHZ);
		glVertex3f(HIGHRIGHT,HIGHBOTTOM,HIGHZ);
		glVertex3f(HIGHRIGHT,HIGHTOP,HIGHZ);
		glEnd();
		glEnable(GL_LIGHT0);
		glEnable(GL_LIGHT1);
		glBlendFunc(GL_ONE,GL_ONE);


		glPushMatrix();
		glTranslatef(HIGHX,HIGHY,HIGHZ);
		glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, grey);

		glPushMatrix();
		drawstring(1,"Rank");
		glTranslatef(HIGHXNAME, 0.0, 0.0);
		drawstring(0,"Name");
		glTranslatef(HIGHXSCORE, 0.0, 0.0);
		drawstring(1,"Score");
		glTranslatef(HIGHXLEVEL, 0.0, 0.0);
		drawstring(1,"Level");
		glPopMatrix();
		glTranslatef(0.0, HIGHYSPACE, 0.0);

		for(i=0;i<10;++i,++h)
		{
			char nametemp[MAXNAMELENGTH];
			strcpy(nametemp,h->name);
			if(enteringline==i+baseshow)
			{
				glMaterialfv(GL_FRONT,GL_AMBIENT_AND_DIFFUSE, red);
				if(gamestate==ENTERHIGH)
					strcat(nametemp,">");
			}
			glPushMatrix();
			drawstring(1,"%d.",i+baseshow+1);
			glTranslatef(HIGHXNAME, 0.0, 0.0);
			drawstring(0,"%s",nametemp);
			glTranslatef(HIGHXSCORE, 0.0, 0.0);
			drawstring(1,"%d",h->score);
			glTranslatef(HIGHXLEVEL, 0.0, 0.0);
			drawstring(1,"%d",h->level);
			glPopMatrix();
			glTranslatef(0.0, HIGHYSPACE, 0.0);
			if(enteringline==i+baseshow)
				glMaterialfv(GL_FRONT,GL_AMBIENT_AND_DIFFUSE, grey);
		}
		glPopMatrix();
		figure();
	}

	SDL_GL_SwapBuffers();


}

void showmatrix(float *f)
{
	printf("%f %f %f %f\n",f[0],f[4],f[8],f[12]);
	printf("%f %f %f %f\n",f[1],f[5],f[9],f[13]);
	printf("%f %f %f %f\n",f[2],f[6],f[10],f[14]);
	printf("%f %f %f %f\n",f[3],f[7],f[11],f[15]);
}

void getpos(int *px,int *py,int mx,int my)
{
vector world,screen;
GLint viewport[4];

	glGetIntegerv(GL_VIEWPORT,viewport);
	screen[0]=(mx-viewport[0])*2.0f/viewport[2]-1.0f;
	screen[1]=(my-viewport[1])*2.0f/viewport[3]-1.0f;
	screen[2]=0.27272727f;
	screen[3]=1.0f;
	matrixXvector(&world,&unprojectmatrix,&screen);
	if(world[3]!=0.0)
	{
		float x,y;
		float z=world[2]/world[3];
		x=(z*world[0]/world[3]-SHIFTX)/SPACING+EX/2;
		y=(z*world[1]/world[3]-SHIFTY)/SPACING+EY/2;
		*px=(int)(x+1000)-1000;
		*py=(int)(y+1000)-1000;
	} else *px=*py=-1;

}

int chainreaction;

int wins[16][4]={
{1,-1,0,1},
{-1,-1,-1,1},
{0,-1,1,1},
{0,-2,0,1},

{1,1,-1,0},
{1,-1,-1,-1},
{1,0,-1,1},
{2,0,-1,0},

{-1,1,0,-1},
{1,1,1,-1},
{0,1,-1,-1},
{0,2,0,-1},

{-1,-1,1,0},
{-1,1,1,1},
{-1,0,1,-1},
{-2,0,1,0},
};
int anymoves(void)
{
unsigned char temp[EY+4][EX+4];
struct element *e;
int i,j,k,t;
int moves;
	memset(temp,0xff,sizeof(temp));
	e=elements[0];
	for(j=0;j<EY;++j)
		for(i=0;i<EX;++i)
			temp[j+2][i+2]=e++ -> type;

	moves=0;
	for(j=2;j<EY+2;++j)
		for(i=2;i<EX+2;++i)
			for(k=0;k<16;++k)
			{
				t=temp[j][i];
				if(t!=temp[j+wins[k][1]][i+wins[k][0]]) continue;
				if(t!=temp[j+wins[k][3]][i+wins[k][2]]) continue;
				++moves;
			}
	return nummoves=moves;
}

void tossall(void)
{
int j;
struct element *e;
	e=elements[0];
	j=EX*EY;
	while(j--) e++ -> vanish=0.999f;
	playsound(BIGDROPSOUND);
}

void randomvector(float *x,float *y,float *z)
{
int ix,iy,iz;
float d;

	do
	{
		ix=(rand()&1023)-512;
		iy=(rand()&1023)-512;
		iz=(rand()&1023)-512;
	} while(ix==0 && iy==0 && iz==0);
	d=1.0f/sqrtf((float)(ix*ix+iy*iy+iz*iz));
	*x=ix*d;
	*y=iy*d;
	*z=iz*d;
*x=*z=0;*y=1.0;
}

void initelement(struct element *e)
{

	memset(e,0,sizeof(*e));
	e->type=(rand()&0xffff)%7;
	e->angle=(rand()&1023)*360.0f/1024.0f;
	e->vanish=1.0;
	e->fall=0.0;
	randomvector(&e->ax,&e->ay,&e->az);
}

void initelements(void)
{
int i,j;
struct element *e;
	e=elements[0];
	j=EX*EY;
	for(i=0;i<j;++i)
		initelement(e++);
}

void replace(void)
{
int i,j,k;
struct element *e,*e2;
int falls[EX];

	for(i=0;i<EX;++i)
		falls[i]=1;
	for(j=EY-1;j>=0;--j)
	{
		for(i=0;i<EX;++i)
		{
			e=elements[j]+i;
			if(e->vanish!=0.0) continue;
			k=j;
			while(--k>=0)
			{
				e2=elements[k]+i;
				if(e2->vanish!=0.0) break;
			}
			if(k>=0)
			{
				*e=*e2;
				e2->vanish=0.0;
				e->fall=j-k;
			} else
			{
				initelement(e);
				e->fall=j+falls[i]++;
			}
		}
	}
}

void addlife(int chain,int len,float x,float y)
{
int value;
int i;
struct scoresprite *ss;
	value=chain+len;
	cumulative=cumulativebuildup+=value;
	cumulativefade=1.0f;
	for(ss=scoresprites,i=0;i<MAXSCORESPRITES;++i,++ss)
	{
		if(ss->fade!=0.0f) continue;
		ss->x=(x-EX/2.0f+0.5f)*SPACING+SHIFTX;
		ss->y=-(y-EY/2.0f)*SPACING+SHIFTY;
		ss->z=0.0f;
		ss->fade=1.0f;
		ss->value=value;
		break;
	}

	score+=value;
	life+=(value)*CREDIT;
	stage+=len;
	if(stage>=NEXTLEVEL)
	{
		stage-=NEXTLEVEL;
		decay+=DECAYADD;
		++level;
	}
}
void endgame(void)
{
struct highscore *h;

	gamestate=GAMEOVER;
	h=highscores+MAXHIGHSCORES;
	while(h>highscores && h[-1].score<score) --h;
	enteringline=h-highscores;
	baseshow=enteringline-5;
	if(baseshow<0) baseshow=0;
	if(baseshow>MAXHIGHSCORES-10) baseshow=MAXHIGHSCORES-10;
	if(enteringline==MAXHIGHSCORES) return;
	memmove(h+1,h,(MAXHIGHSCORES-enteringline)*sizeof(struct highscore));
	strcpy(h->name,lastentered);
	h->score=score;
	h->level=level;
	gamestate=ENTERHIGH;
}
void declife()
{
	timeindicator-=decay*5.0f;
	life-=decay;
	if(life<0.0)
	{
		life=0.0;
		if(gamestate!=GAMEOVER)
		{
			endgame();
			playsound(GAMEOVERSOUND);
		}
	}
}

int findwins(int justchecking)
{
int i,j,k;
struct element *e;
int hadsome=0;
float x,y;

	for(j=0;j<EY;++j)
	{
		k=0;
		for(i=1;i<EX+1;++i)
		{
			e=elements[j]+i;
			if(i<EX && e->type==e[-1].type) ++k;
			else if(k>=2)
			{
				hadsome=1;
				if(!justchecking)
				{
					x=i-1-k/2.0f;
					y=j+0.5f;
					addlife(chainreaction,k-1,x,y);
				}
				++k;
				while(k) e[-k--].vanish=0.999f;
				k=0;
			} else k=0;
		}
	}

	for(j=0;j<EX;++j)
	{
		k=0;
		for(i=1;i<EY+1;++i)
		{
			e=elements[i]+j;
			if(i<EY && e->type==e[-EX].type) ++k;
			else if(k>=2)
			{
				hadsome=1;
				if(!justchecking)
				{
					x=j;
					y=i-0.5f-k/2.0f;
					addlife(chainreaction,k-1,x,y);
				}
				++k;
				while(k) e[-EX*k--].vanish=0.999f;
				k=0;
			} else k=0;
		}
	}
	if(!justchecking)
	{
		if(hadsome)
		{
			playsound(ROWSOUND);
			++chainreaction;
		} else chainreaction=0,cumulativebuildup=0;
	}
	return hadsome;
}

#define SWAPMAX 4
char swapfifo[SWAPMAX][4];
int swapput=0,swaptake=0;
void addswap(int px,int py,int dx,int dy)
{
char *p;
	if(((swapput+1) & (SWAPMAX-1)) == swaptake) return;
	p=swapfifo[swapput++];
	swapput&=SWAPMAX-1;
	*p++=px;
	*p++=py;
	*p++=dx;
	*p=dy;
}

int tryswap(void)
{
struct element t;
struct element *e1,*e2;
char *p;
int px,py,dx,dy;
	if(swapput==swaptake) return 0;

	playsound(SWAPSOUND);
	p=swapfifo[swaptake++];
	swaptake&=SWAPMAX-1;
	px=*p++;
	py=*p++;
	dx=*p++;
	dy=*p;
	e1=elements[py]+px;
	e2=elements[py+dy]+px+dx;
	t=*e1;
	*e1=*e2;
	*e2=t;
	e1->dx=dx;
	e1->dy=-dy;
	e1->swapping=1;
	e2->dx=-dx;
	e2->dy=dy;
	e2->swapping=1;
	return 1;
}


#define FALLRATE 0.02f
#define VANISHRATE 0.05f

int dropsound;
int waspicked;
void illegal(struct element *e1,struct element *e2)
{
struct element t;
	t=*e1;
	*e1=*e2;
	*e2=t;
	e1->dx=-e1->dx;
	e1->dy=-e1->dy;
	e1->swapping=1;
	e2->dx=-e2->dx;
	e2->dy=-e2->dy;
	e2->swapping=1;
	waspicked=0;
}

int alertcounter;

enum actionstates {
ACTION_LOOKING,
ACTION_WAITING,
ACTION_SWAPPING,
ACTION_UNSWAPPING,
ACTION_REMOVING,
ACTION_DROPPING,
};

int actionmode;
void action()
{
struct element *e,*le,*e1=0,*e2=0;
int i;
int hadsome;
struct scoresprite *ss;

	if(gamestate==PLAYING && life<ALARMLEVEL*50*decay)
	{
		--alertcounter;
		if(alertcounter<=0)
		{
			playsound(ALERTSOUND);
			alertcounter=50;
		}
	}

	for(i=0,ss=scoresprites;i<MAXSCORESPRITES;++i,++ss)
	{
		ss->fade-=SCORESPRITEFADE;
		if(ss->fade<0.0) ss->fade=0.0;
	}

	cumulativefade-=SCORESPRITEFADE;
	if(cumulativefade<0.0) cumulativefade=0.0;

	e=elements[0];
	le=e+EX*EY;
	while(e<le) e++ ->angle+=3.0;

	switch(actionmode)
	{
	case ACTION_LOOKING:
		if(findwins(0))
			actionmode=ACTION_REMOVING;
		else
			if(anymoves())
				actionmode=ACTION_WAITING;
			else
				{tossall();actionmode=ACTION_REMOVING;}
		break;
	case ACTION_WAITING:
		if(gamestate!=PLAYING) break;
		declife();
		if(tryswap()) actionmode=ACTION_SWAPPING;
		break;
	case ACTION_UNSWAPPING:
		declife();
	case ACTION_SWAPPING:
		e=elements[0];
		hadsome=0;
		while(e<le)
		{
			if(e->swapping)
			{
				hadsome=1;
				++e->swapping;
				if(e->swapping==SWAPTIME)
				{
					e->swapping=0;
					e1=e2;
					e2=e;
					hadsome=2;
				}
			}
			++e;
		}
		if(hadsome==2)
		{
			if(findwins(0))
				actionmode=ACTION_REMOVING;
			else if(actionmode==ACTION_SWAPPING)
			{
				playsound(ILLEGALSOUND);
				illegal(e1,e2);
				actionmode=ACTION_UNSWAPPING;
			} else
				actionmode=ACTION_WAITING;
		}
		break;
	case ACTION_REMOVING:
		hadsome=0;
		e=elements[0];
		while(e<le)
		{
			if(e->vanish<1.0)
			{
				e->vanish-=VANISHRATE;
				if(e->vanish<=0.0)
				{
					e->vanish=0.0;
					++hadsome;
				}
			}
			++e;
		}
		if(hadsome)
		{
			replace();
			dropsound=1;
			actionmode=ACTION_DROPPING;
		}
		break;
	case ACTION_DROPPING:
		e=elements[0];
		hadsome=0;
		while(e<le)
		{
			if(e->fall>0.0)
			{
				++hadsome;
				e->fall-=e->speed;
				e->speed+=FALLRATE;
				if(e->fall<=0.0)
				{
					e->fall=e->speed=0.0;
					if(dropsound)
					{
						playsound(DROPSOUND);
						dropsound=0;
					}
				}
			}
			++e;
		}
		if(!hadsome) actionmode=ACTION_LOOKING;
		break;
	}
}

#define MAXCODES 64
static int downcodes[MAXCODES];
static int pressedcodes[MAXCODES];
static int numcodes=0,numpressed=0;

void processkey(int key,int state)
{
int i;
	if(state)
	{
		if(numpressed<MAXCODES) pressedcodes[numpressed++]=key;
		for(i=0;i<numcodes;++i)
			if(downcodes[i]==key) return;
		if(numcodes<MAXCODES)
			downcodes[numcodes++]=key;
	} else
	{
		for(i=0;i<numcodes;)
		{
			if(downcodes[i]==key)
				downcodes[i]=downcodes[--numcodes];
			else
				++i;
		}

	}
}
int IsDown(int key)
{
int i;
	for(i=0;i<numcodes;++i)
		if(downcodes[i]==key) return 1;
	return 0;
}

int WasPressed(int key)
{
int i;
	for(i=0;i<numpressed;++i)
		if(pressedcodes[i]==key) return 1;
	return 0;
}

void writehighscores();

void typedkey(int key)
{
char *p;
	if(key<0 || key>0x7f) return;
	p=highscores[enteringline].name;
	if(key==10 || key==13)
	{
		gamestate=GAMEOVER;
		writehighscores();
	}
	else if(key==8)
	{
		if(*p) p[strlen(p)-1]=0;
	} else
		if(strlen(p)<MAXNAMELENGTH-1)
		{
			p+=strlen(p);
			*p++=key;
			*p=0;
		}
	strcpy(lastentered,highscores[enteringline].name);
}


int downx,downy;
int isdown;
void down(int x,int y)
{
	downx=x;
	downy=y;
	isdown=1;
}

#define DIST 20
#define ABS(x) (((x)<0) ? (-(x)) : (x))
void moved(int x,int y)
{
int dx,dy;
int px,py;

	getpos(&cposx,&cposy,x,y);
	if(!isdown) return;
	dx=x-downx;
	dy=y-downy;
	if(dx*dx+dy*dy<DIST*DIST) return;
	isdown=0;
	if(ABS(dx)>ABS(dy))
	{
		if(dx<0) dx=-1;
		else dx=1;
		dy=0;
	} else
	{
		if(dy<0) dy=-1;
		else dy=1;
		dx=0;
	}
	getpos(&px,&py,downx,downy);
/*
#define FIX 8.0
	px=(EX>>1)+((float)downx/sizex-0.5)*FIX;
	py=(EY>>1)+((float)downy/sizey-0.5)*FIX;
*/

	if(px<0 || px>=EX || py<0 || py>=EY) return;
	if(px+dx<0 || px+dx>=EX || py+dy<0 || py+dy>=EY) return;
	addswap(px,py,dx,dy);

}

void up(int x,int y)
{
	isdown=0;
}


void initgame(void)
{
int i;
	life=STARTLIFE;
	decay=INITIALDECAY;
	do initelements(); while(findwins(1));
	chainreaction=swapput=swaptake=0;
	stage=0;
	score=0;
	level=1;
	gamestate=PLAYING;
	cumulative=0;
	cumulativebuildup=0;
	cumulativefade=0.0;
	alertcounter=0;
	baseshow=0;
	enteringline=-1;
	for(i=0;i<MAXSCORESPRITES;++i) scoresprites[i].fade=0.0;
	actionmode=ACTION_LOOKING;
}

#define INTERVAL 5

int hc=0;
#ifdef SMW_CHANGES
void thandler(int val)
{
	signal(SIGALRM,thandler);
	++hc;
}
#endif

static void event_loop( void )
{
char exitflag=0;
int nframes=0;
int freeze=0;
SDL_Event event;
int code;


	Uint32 ticks = SDL_GetTicks();

	while(!exitflag)
	{
		Uint32 new_ticks = SDL_GetTicks();
		Uint32 delta = new_ticks - ticks;
		if (delta > (1000/44))
		{
			action();
			ticks = new_ticks;
		}
		++nframes;
		draw();
#define DELTA 1.0

		if(WasPressed(SDLK_F1)) initgame();
		if(WasPressed(SDLK_F2)) playsound(BIGDROPSOUND);
		if(WasPressed(SDLK_F3)) playsound(GAMEOVERSOUND);
		if(WasPressed(SDLK_F4)) playsound(ROWSOUND);
		if(WasPressed(SDLK_F5)) playsound(ILLEGALSOUND);
		if(WasPressed(SDLK_F6)) playsound(ALERTSOUND);

#if 0
		if(gamestate==PLAYING && WasPressed(XK_p)) freeze=!freeze;
#endif
		if(gamestate==GAMEOVER && WasPressed(SDLK_SPACE)) initgame();
		//		if(WasPressed(XK_p)) freeze=!freeze;

		numpressed=0;

		while(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
			case SDL_MOUSEMOTION:
				mousex=event.motion.x;
				mousey=event.motion.y;
				moved(mousex, mousey);
				break;
			case SDL_MOUSEBUTTONDOWN:
				mousex=event.button.x;
				mousey=event.button.y;
				down(mousex, mousey);
				break;
			case SDL_MOUSEBUTTONUP:
				mousex=event.button.x;
				mousey=event.button.y;
				up(mousex, mousey);
				break;
			case SDL_KEYDOWN:
				code=event.key.keysym.sym;
				if(code==SDLK_ESCAPE) exitflag=1;
				processkey(code, 1);
				if(gamestate==ENTERHIGH) typedkey(code);
				break;
			case SDL_KEYUP:
				code=event.key.keysym.sym;
				processkey(code, 0);
				break;
			case SDL_VIDEORESIZE:
				resize(event.resize.w, event.resize.h);
				break;
			case SDL_QUIT:
				exitflag=1;
				break;				
// handle resize
			}
		}
	}
}

void makehighname(char *p)
{
	char *homedir = getenv("HOME");
	if (homedir)
		sprintf(p,"%s/%s",homedir,HIGHSCOREFILENAME);
	else
		sprintf(p,"%s",HIGHSCOREFILENAME);
}

void readhighscores()
{
FILE *f;
int i,n;
char tempname[256];
int score,level;
struct highscore *h;

	memset(highscores,0,sizeof(highscores));
	makehighname(tempname);
	f=fopen(tempname,"r");
	if(!f) return;
	for(h=highscores,i=0;i<MAXHIGHSCORES;++i,++h)
	{
		n=fscanf(f,"%s %d %d",tempname,&score,&level);
		if(n==3)
		{
			tempname[MAXNAMELENGTH-1]=0;
			if(*tempname=='.') *tempname=0;
			strcpy(h->name,tempname);
			h->score=score;
			h->level=level;
		}
	}
}
void writehighscores()
{
FILE *f;
int i;
char tempname[256];
struct highscore *h;

	makehighname(tempname);
	f=fopen(tempname,"w");
	if(!f) return;
	h=highscores;
	for(h=highscores,i=0;i<MAXHIGHSCORES;++i,++h)
	{
		fprintf(f,"%s %d %d\n",h->name[0] ? h->name : ".",h->score,h->level);
	}
	fclose(f);
}


int main( int argc, char *argv[] )
{
SDL_Surface *screen;

//	if(fork()) return;
	readhighscores();

	srand(time(NULL));
	soundinit();
	initgame();
	gamestate=GAMEOVER;
	eye[0]=10.0;
	eye[1]=0.0;
	eye[2]=0.0;
	eye[3]=0.0;


	SDL_Init(SDL_INIT_VIDEO);
	sizex = 800;
	sizey = 600;

	screen = setvideomode(sizex, sizey);
	SDL_WM_SetCaption("glJewel", "gljewel");

	resize(sizex, sizey);

//	glLightfv(GL_LIGHT0, GL_POSITION, lightpos);
	glEnable(GL_CULL_FACE);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glLightfv(GL_LIGHT0,GL_SPECULAR,white);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,grey);
	glLightfv(GL_LIGHT1,GL_SPECULAR,white2);
	glLightfv(GL_LIGHT1,GL_DIFFUSE,grey2);
	glLightfv(GL_LIGHT2,GL_SPECULAR,white);
	glLightfv(GL_LIGHT2,GL_DIFFUSE,grey);
	glEnable(GL_DEPTH_TEST);

	glShadeModel( GL_FLAT );
	glClearColor( 0.0, 0.0, 0.0, 1.0 );
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDisable(GL_BLEND);
//	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glBlendFunc(GL_ONE, GL_ONE);

#ifndef HAVE_GL_VERTEX_BUFFERS
	initGlVboExtension();

#endif
	glLineWidth(2.0);
	glDisable(GL_LINE_SMOOTH);

	initfont();

	objectlists = glGenLists(9);
	glNewList(objectlists+0, GL_COMPILE);
	makebucky(0.9*SCALE); // blue
	glEndList();

	glNewList(objectlists+1, GL_COMPILE);
	makebcube(SCALE); // orange
	glEndList();

	glNewList(objectlists+2, GL_COMPILE);
	makepyramid(0.7*SCALE); // yellow
	glEndList();
	create_pyramid(&shapes[2]);
	shapes[2].create(0.7f*SCALE);

	glNewList(objectlists+3, GL_COMPILE);
	makeicosahedron(0,SCALE); // magenta
	glEndList();

	glNewList(objectlists+4, GL_COMPILE);
	makecylinder(0.8f*SCALE); // green
	glEndList();

	glNewList(objectlists+5, GL_COMPILE);
	makediamond(0.9f*SCALE); // red
	glEndList();

	glNewList(objectlists+6, GL_COMPILE);
	makeuvsphere(0.9f*SCALE); // white
	glEndList();

	glNewList(objectlists+7, GL_COMPILE);
	makedots(0.95f*SCALE);
	glEndList();

	glNewList(objectlists+8, GL_COMPILE);
	makespiky(blue, 1.0);
	glEndList();

	event_loop();
	soundfree();
	writehighscores();
	SDL_Quit();
	return 0;
}
