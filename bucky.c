/**
 * @file bucky.c
 * @brief The buckyball-ish module.
 *
 * Copyright 2001, 2008 David Ashley  <dashxdr@gmail.com>
 * Copyright 2008 Stephen M. Webb  <stephen.webb@bregmasoft.ca>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of Version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "bucky.h"

#if defined(_WIN32) && !defined(__CYGWIN__)
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif

#include <GL/gl.h>

/**
 * Special predefined values for drawing buckyballs.
 */
static GLfloat coords[60][3] = {
{-0.449358f,0.730026f,0.514918f},
{-0.277718f,0.201774f,0.939234f},
{-0.277718f,-0.201774f,0.939234f},
{-0.555436f,0.403548f,0.727076f},
{-0.555436f,-0.403548f,0.727076f},
{-0.833155f,0.201774f,0.514918f},
{-0.833155f,-0.201774f,0.514918f},
{0.106079f,-0.326477f,0.939234f},
{0.212158f,-0.652955f,0.727076f},
{-0.449358f,-0.730026f,0.514918f},
{-0.065560f,-0.854729f,0.514918f},
{0.343279f,0.000000f,0.939234f},
{0.686557f,0.000000f,0.727076f},
{0.555436f,-0.652955f,0.514918f},
{0.792636f,-0.326477f,0.514918f},
{0.661515f,0.730026f,-0.171639f},
{0.898715f,0.403548f,-0.171639f},
{0.489876f,0.854729f,0.171639f},
{0.964275f,0.201774f,0.171639f},
{0.555436f,0.652955f,0.514918f},
{0.792636f,0.326477f,0.514918f},
{-0.489876f,0.854729f,-0.171639f},
{-0.106079f,0.979432f,-0.171639f},
{-0.661515f,0.730026f,0.171639f},
{0.106079f,0.979432f,0.171639f},
{-0.065560f,0.854729f,0.514918f},
{-0.964275f,-0.201774f,-0.171639f},
{-0.964275f,0.201774f,-0.171639f},
{-0.898715f,-0.403548f,0.171639f},
{-0.898715f,0.403548f,0.171639f},
{-0.106079f,-0.979432f,-0.171639f},
{-0.489876f,-0.854729f,-0.171639f},
{0.106079f,-0.979432f,0.171639f},
{-0.661515f,-0.730026f,0.171639f},
{0.898715f,-0.403548f,-0.171639f},
{0.661515f,-0.730026f,-0.171639f},
{0.964275f,-0.201774f,0.171639f},
{0.489876f,-0.854729f,0.171639f},
{0.065560f,0.854729f,-0.514918f},
{0.449358f,0.730026f,-0.514918f},
{-0.792636f,0.326477f,-0.514918f},
{-0.555436f,0.652955f,-0.514918f},
{-0.555436f,-0.652955f,-0.514918f},
{-0.792636f,-0.326477f,-0.514918f},
{0.449358f,-0.730026f,-0.514918f},
{0.065560f,-0.854729f,-0.514918f},
{0.833155f,0.201774f,-0.514918f},
{0.833155f,-0.201774f,-0.514918f},
{0.277718f,0.201774f,-0.939234f},
{-0.106079f,0.326477f,-0.939234f},
{0.555436f,0.403548f,-0.727076f},
{-0.212158f,0.652955f,-0.727076f},
{-0.343279f,0.000000f,-0.939234f},
{-0.686557f,0.000000f,-0.727076f},
{-0.106079f,-0.326477f,-0.939234f},
{-0.212158f,-0.652955f,-0.727076f},
{0.277718f,-0.201774f,-0.939234f},
{0.555436f,-0.403548f,-0.727076f},
{0.106079f,0.326477f,0.939234f},
{0.212158f,0.652955f,0.727076f}
};

/**
 * Special adjustment value.  Only used to draw buckyballs.
 */
static float buckyfix;

/**
 * Generates an OpenGL 1.1 vertex from an index into the bucky coord table.
 * Only used to draw buckyballs.
 */
static void point(int n)
{
	glVertex3f(coords[n][0]*buckyfix,coords[n][1]*buckyfix,coords[n][2]*buckyfix);
}

/**
 * Calculates the normal of a plane defined by three points?  Only used to draw
 * buckyballs.
 */
static void norm(int p1,int p2,int p3)
{
float nx,ny,nz;
float x1,y1,z1;
float x2,y2,z2;

	x1=coords[p2][0]-coords[p1][0];
	y1=coords[p2][1]-coords[p1][1];
	z1=coords[p2][2]-coords[p1][2];

	x2=coords[p3][0]-coords[p1][0];
	y2=coords[p3][1]-coords[p1][1];
	z2=coords[p3][2]-coords[p1][2];

	nx=y1*z2-y2*z1;
	ny=x2*z1-x1*z2;
	nz=x1*y2-x2*y1;
	glNormal3f(nx,ny,nz);

}

/**
 * Draws a hexagon defined by 6 points.  Only used to draw buckyballs.
 */
static void hex(int p1,int p2,int p3,int p4,int p5,int p6)
{
	norm(p1,p3,p2);
	glPolygonMode(GL_FRONT,GL_FILL);
	glBegin(GL_POLYGON);
	point(p6);
	point(p5);
	point(p4);
	point(p3);
	point(p2);
	point(p1);
	glEnd();
	glPolygonMode(GL_FRONT,GL_FILL);
}

/**
 * Draws a pentagon defined by 5 points.  Only used to draw buckyballs.
 */
static void pent(int p1,int p2,int p3,int p4,int p5)
{
	glBegin(GL_TRIANGLE_STRIP);
	norm(p1,p3,p2);
	point(p1);
	point(p5);
	point(p2);
	point(p4);
	point(p3);
	glEnd();
}

/**
 * Draws a buckyball-ish (a buckyball is 60-sided, this baby is 32-sided).
 */
void makebucky(float size)
{
	buckyfix=size;

	glEnable(GL_NORMALIZE);
	glDisable(GL_TEXTURE_2D);

	hex(2,7,8,10,9,4);
	hex(1,2,4,6,5,3);
	hex(7,11,12,14,13,8);
	hex(9,10,32,30,31,33);
	hex(5,6,28,26,27,29);
	hex(0,25,59,58,1,3);
	hex(11,58,59,19,20,12);
	hex(21,22,24,25,00,23);
	hex(30,32,37,35,44,45);
	hex(26,28,33,31,42,43);
	hex(15,17,24,22,38,39);
	hex(15,16,18,20,19,17);
	hex(38,51,49,48,50,39);
	hex(13,14,36,34,35,37);
	hex(16,46,47,34,36,18);
	hex(21,23,29,27,40,41);
	hex(40,53,52,49,51,41);
	hex(44,57,56,54,55,45);
	hex(46,50,48,56,57,47);
	hex(42,55,54,52,53,43);



	pent(1,58,11,7,2);
	pent(8,13,37,32,10);
	pent(4,9,33,28,6);
	pent(0,3,5,29,23);
	pent(17,19,59,25,24);
	pent(12,20,18,36,14);
	pent(30,45,55,42,31);
	pent(21,41,51,38,22);
	pent(48,49,52,54,56);
	pent(15,39,50,46,16);
	pent(34,47,57,44,35);
	pent(26,43,53,40,27);
}


