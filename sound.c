/**
 * @file matrix.c
 * @brief The implementation of the gljewel sound module.
 *
 * Copyright 2001, 2008 David Ashley  <dashxdr@gmail.com>
 * Copyright 2008 Stephen M. Webb  <stephen.webb@bregmasoft.ca>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of Version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if defined(_WIN32) && !defined(__CYGWIN__)
#  define WIN32_LEAN_AND_MEAN 1
#  include <windows.h>
#else
# include <unistd.h>
# include <sys/ioctl.h>
# include <sys/time.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>

#include <SDL/SDL.h>

#include "misc.h"

#define MIXMAX 16
#define SNDFRAGMENT 1024
#define MAXSOUNDCOMMANDS 32
static int soundcommands[MAXSOUNDCOMMANDS];
static int soundtake,soundput;
static int sndplaying[MIXMAX],sndposition[MIXMAX];

static char dirlist[]="data,"DATA_DIR;

#define NUMSOUNDS	(sizeof(soundnames)/sizeof(char*))
#define MIXMAX 16

#define SOUND_EXIT -2
#define SOUND_QUIET -1

static char *soundnames[] =
{
"row.raw", // 0
"drop.raw", // 1
"bigdrop.raw", // 2
"swap.raw", // 3
"gameover.raw", // 4
"alert.raw", // 5
"illegal.raw", // 6
};
typedef struct sample
{
	short *data;
	int len;
} sample;

static sample samples[NUMSOUNDS];

static int soundworking=0;
static int fragment;
static int *soundbuffer;
static int soundbufferlen;


static int readsound(int num)
{
char name[256],*p1,*p2,ch;
int i,size,len;
FILE* file = NULL;
	p1=dirlist;
	for(;;)
	{
		p2=name;
		while(*p1 && (ch=*p1++)!=',')
			*p2++=ch;
		if(p2>name && p2[-1]!='/') *p2++='/';
		strcpy(p2,soundnames[num]);
		file=fopen(name,"rb");
		if(file) break;
		if(!*p1)
		{
			samples[num].len=-1;
			return 0;
		}
	}
	size=fseek(file,0,SEEK_END);
	if (size <= 0)
	{
		samples[num].len=-1;
		return 0;
	}
	fseek(file,0,SEEK_SET);
	size>>=1;
	len=samples[num].len=(size+fragment-1)/fragment;
	len*=fragment;
	p1=(void *)(samples[num].data=malloc(len<<1));
	if(p1)
	{
		memset(p1,0,len<<1);
		i=fread(p1,sizeof(char),size<<1,file);
	} else
		samples[num].data=0;
	fclose(file);
	return 0;
}

static void fillaudio(void *udata, Uint8 *bp,int len)
{
char com;
short *p;
int i,j,*ip;
int which;
Sint16 *buffer = (void *)bp;

	len/=2;

	while(soundtake!=soundput)
	{
		com=soundcommands[soundtake];
		soundtake=(soundtake+1)&(MAXSOUNDCOMMANDS-1);
		if(com==SOUND_QUIET) {memset(sndposition,0,sizeof(sndposition));continue;}
		if(com<NUMSOUNDS)
		{
			for(i=0;i<MIXMAX;++i)
				if(!sndposition[i])
				{
					sndposition[i]=1;
					sndplaying[i]=com;
					break;
				}
		}
	}
	memset(soundbuffer,0,soundbufferlen);
	for(i=0;i<MIXMAX;++i)
	{
		if(!sndposition[i]) continue;
		which=sndplaying[i];
		if(sndposition[i]==samples[which].len)
		{
			sndposition[i]=0;
			continue;
		}
		p=samples[which].data;
		if(!p) continue;
		p+=len*(sndposition[i]++ -1);
		ip=soundbuffer;
		j=len;
		while(j--) *ip++ += *p++;
	}
	j=len;
	ip=soundbuffer;;
	while(j--)
	{
		int t;
		t=*ip++;
		t = (t>0x7fff) ? 0x7fff : (t<-0x8000) ? -0x8000 : t;
		*buffer++ = t;
	}
}


int soundinit(void)
{
SDL_AudioSpec wanted;
int i;

	soundtake=soundput=0;
	memset(sndposition,0,sizeof(sndposition));
	memset(sndplaying,0,sizeof(sndplaying));
	fragment=SNDFRAGMENT;
	soundbufferlen=fragment*sizeof(int);
	soundbuffer=malloc(soundbufferlen);
	if(!soundbuffer) return -2;

	memset(&wanted,0,sizeof(wanted));
	wanted.freq=22050;
	wanted.channels=1;
	wanted.format=AUDIO_S16;
	wanted.samples=fragment;
	wanted.callback=fillaudio;
	wanted.userdata=0;

	if(SDL_OpenAudio(&wanted,0)<0)
	{
		fprintf(stderr,"Couldn't open audio: %s\n",SDL_GetError());
		return -1;
	}
	soundworking=1;

	for(i=0;i<NUMSOUNDS;++i)
		readsound(i);

	SDL_PauseAudio(0);
	return 0;
}

void soundfree(void)
{
	if(soundbuffer)
	{
		free(soundbuffer);
		soundbuffer = 0;
	}
	SDL_PauseAudio(0);
}

void playsound(int n)
{
	soundcommands[soundput]=n;
	soundput=(soundput+1)&(MAXSOUNDCOMMANDS-1);
}
