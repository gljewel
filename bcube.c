/**
 * @file bcube.c
 * @brief The cheese cube module.
 *
 * Copyright 2001, 2008 David Ashley  <dashxdr@gmail.com>
 * Copyright 2008 Stephen M. Webb  <stephen.webb@bregmasoft.ca>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of Version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "bcube.h"

#if defined(_WIN32) && !defined(__CYGWIN__)
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif

#include <GL/gl.h>

void makebcube(float scale)
{
float sizex,sizey,sizez;
float bsizex,bsizey,bsizez;
float bevel;

	sizex=sizey=sizez=0.6f*scale;
	bevel=0.15f*scale;
	bsizex=sizex+bevel;
	bsizey=sizey+bevel;
	bsizez=sizez+bevel;
	glDisable(GL_TEXTURE_2D);
	glEnable(GL_NORMALIZE);

	glBegin(GL_QUADS);
	glNormal3f( 0.0, sizey, 0.0);
	glVertex3f( sizex, bsizey, sizez);
	glVertex3f( sizex, bsizey,-sizez);
	glVertex3f(-sizex, bsizey,-sizez);
	glVertex3f(-sizex, bsizey, sizez);

	glNormal3f( 0.0, 0.0, sizez);
	glVertex3f( sizex, sizey, bsizez);
	glVertex3f(-sizex, sizey, bsizez);
	glVertex3f(-sizex,-sizey, bsizez);
	glVertex3f( sizex,-sizey, bsizez);


	glNormal3f( 0.0, 0.0,-sizez);
	glVertex3f(-sizex,-sizey,-bsizez);
	glVertex3f(-sizex, sizey,-bsizez);
	glVertex3f( sizex, sizey,-bsizez);
	glVertex3f( sizex,-sizey,-bsizez);

	glNormal3f( sizex, 0.0, 0.0);
	glVertex3f( bsizex, sizey, sizez);
	glVertex3f( bsizex,-sizey, sizez);
	glVertex3f( bsizex,-sizey,-sizez);
	glVertex3f( bsizex, sizey,-sizez);

	glNormal3f(-sizex, 0.0, 0.0);
	glVertex3f(-bsizex,-sizey,-sizez);
	glVertex3f(-bsizex,-sizey, sizez);
	glVertex3f(-bsizex, sizey, sizez);
	glVertex3f(-bsizex, sizey,-sizez);

	glNormal3f( 0.0,-sizey, 0.0);
	glVertex3f(-sizex,-bsizey,-sizez);
	glVertex3f( sizex,-bsizey,-sizez);
	glVertex3f( sizex,-bsizey, sizez);
	glVertex3f(-sizex,-bsizey, sizez);

//	setmaterial(blue);

	glNormal3f( 0.0, sizey, sizez);
	glVertex3f( -sizex, bsizey, sizez);
	glVertex3f( -sizex, sizey, bsizez);
	glVertex3f(  sizex, sizey, bsizez);
	glVertex3f(  sizex, bsizey, sizez);

	glNormal3f( sizex, 0.0, sizez);
	glVertex3f( bsizex, sizey, sizez);
	glVertex3f( sizex, sizey, bsizez);
	glVertex3f( sizex, -sizey, bsizez);
	glVertex3f( bsizex, -sizey, sizez);

	glNormal3f( sizex, sizey, 0.0);
	glVertex3f(bsizex, sizey, -sizez);
	glVertex3f(sizex, bsizey, -sizez);
	glVertex3f(sizex, bsizey,  sizez);
	glVertex3f(bsizex, sizey,  sizez);

	glNormal3f( 0.0, -sizey, -sizez);
	glVertex3f( -sizex, -bsizey, -sizez);
	glVertex3f( -sizex, -sizey, -bsizez);
	glVertex3f(  sizex, -sizey, -bsizez);
	glVertex3f(  sizex, -bsizey, -sizez);

	glNormal3f( -sizex, 0.0, -sizez);
	glVertex3f( -bsizex,  sizey, -sizez);
	glVertex3f( -sizex,  sizey, -bsizez);
	glVertex3f( -sizex, -sizey, -bsizez);
	glVertex3f( -bsizex, -sizey, -sizez);

	glNormal3f( -sizex, -sizey, 0.0);
	glVertex3f(-bsizex, -sizey,  -sizez);
	glVertex3f(-sizex, -bsizey,  -sizez);
	glVertex3f(-sizex, -bsizey, sizez);
	glVertex3f(-bsizex, -sizey, sizez);

	glNormal3f( 0.0, -sizey, sizez);
	glVertex3f(  sizex, -bsizey, sizez);
	glVertex3f(  sizex, -sizey, bsizez);
	glVertex3f( -sizex, -sizey, bsizez);
	glVertex3f( -sizex, -bsizey, sizez);

	glNormal3f( 0.0, sizey, -sizez);
	glVertex3f( -sizex, sizey, -bsizez);
	glVertex3f( -sizex, bsizey, -sizez);
	glVertex3f(  sizex, bsizey, -sizez);
	glVertex3f(  sizex, sizey, -bsizez);

	glNormal3f( -sizex, 0.0, sizez);
	glVertex3f( -bsizex, -sizey, sizez);
	glVertex3f( -sizex, -sizey, bsizez);
	glVertex3f( -sizex, sizey, bsizez);
	glVertex3f( -bsizex,sizey, sizez);

	glNormal3f( sizex, 0.0, -sizez);
	glVertex3f( sizex, sizey, -bsizez);
	glVertex3f( bsizex, sizey, -sizez);
	glVertex3f( bsizex, -sizey, -sizez);
	glVertex3f( sizex,-sizey, -bsizez);

	glNormal3f( -sizex, sizey, 0.0);
	glVertex3f( -bsizex,sizey, sizez);
	glVertex3f( -sizex, bsizey, sizez);
	glVertex3f( -sizex, bsizey, -sizez);
	glVertex3f( -bsizex, sizey, -sizez);

	glNormal3f( sizex, -sizey, 0.0);
	glVertex3f( sizex, -bsizey, -sizez);
	glVertex3f( bsizex, -sizey, -sizez);
	glVertex3f( bsizex, -sizey, sizez);
	glVertex3f( sizex, -bsizey, sizez);

	glEnd();

//	setmaterial(red);
	glBegin(GL_TRIANGLES);

	glNormal3f(sizex,sizey,sizez);
	glVertex3f(bsizex,sizey,sizez);
	glVertex3f(sizex,bsizey,sizez);
	glVertex3f(sizex,sizey,bsizez);

	glNormal3f(-sizex,sizey,sizez);
	glVertex3f(-sizex,bsizey,sizez);
	glVertex3f(-bsizex,sizey,sizez);
	glVertex3f(-sizex,sizey,bsizez);

	glNormal3f(-sizex,-sizey,sizez);
	glVertex3f(-bsizex,-sizey,sizez);
	glVertex3f(-sizex,-bsizey,sizez);
	glVertex3f(-sizex,-sizey,bsizez);

	glNormal3f(sizex,-sizey,sizez);
	glVertex3f(sizex,-bsizey,sizez);
	glVertex3f(bsizex,-sizey,sizez);
	glVertex3f(sizex,-sizey,bsizez);


	glNormal3f(-sizex,-sizey,-sizez);
	glVertex3f(-sizex,-sizey,-bsizez);
	glVertex3f(-sizex,-bsizey,-sizez);
	glVertex3f(-bsizex,-sizey,-sizez);

	glNormal3f(sizex,-sizey,-sizez);
	glVertex3f(sizex,-sizey,-bsizez);
	glVertex3f(bsizex,-sizey,-sizez);
	glVertex3f(sizex,-bsizey,-sizez);

	glNormal3f(sizex,sizey,-sizez);
	glVertex3f(sizex,sizey,-bsizez);
	glVertex3f(sizex,bsizey,-sizez);
	glVertex3f(bsizex,sizey,-sizez);

	glNormal3f(-sizex,sizey,-sizez);
	glVertex3f(-sizex,sizey,-bsizez);
	glVertex3f(-bsizex,sizey,-sizez);
	glVertex3f(-sizex,bsizey,-sizez);

	glEnd();
}

