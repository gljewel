/**
 * @file text.c
 * @brief The text display module for gljewel
 *
 * Copyright 2001, 2008 David Ashley  <dashxdr@gmail.com>
 * Copyright 2008 Stephen M. Webb  <stephen.webb@bregmasoft.ca>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of Version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "text.h"

#if defined(_WIN32) && !defined(__CYGWIN__)
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif
#include <errno.h>
#include <GL/gl.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

typedef unsigned int uint32;

/* Maps ASCII characters to indexes into font texture. */
static int lmap[128];
#define UNDEFINED_CHAR 0xff

void init_font_map()
{
	static char *fontcharacters="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.!:>^@() +";
	char *p;
	int i = 0;

	memset(lmap, UNDEFINED_CHAR, sizeof(lmap));
	for (p = fontcharacters; *p; ++p)
	{
		lmap[*p] = i++;
	}
}

static GLint fontlist;

/**
 * A loaded image.
 */
struct pic {
	unsigned char *image;
	int w,h;
};

#define MAXCHARS 128
static struct letter {
	unsigned char px,py,sx,sy;
	float advance;
} letters[MAXCHARS];


static unsigned int textures[10];
#define T_WIDTH 256
#define T_HEIGHT T_WIDTH


void drawstring(int align, char *fmt, ...)
{
	int t;
	char temp[128],*p;
	float advance;
	va_list ap;

	va_start(ap, fmt);
	vsprintf(temp,fmt,ap);
	va_end(ap);

	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,textures[0]);

	glNormal3f( 0.0, 0.0, 1.0);

	if(align)
	{
		advance=0.0;
		p=temp;
		while(*p)
		{
			if((t=lmap[(int)*p++])!=-1)
				advance+=letters[t].advance;
		}
		if(align==1) advance/=2.0;
		glTranslatef(-advance,0.0, 0.0);
	}
	p=temp;
	while(*p)
	{
		if((t=lmap[(int)*p++])!=-1)
			glCallList(fontlist+t);
	}
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
}


static FILE* ppmh;
static int ppmin;
static unsigned char ppmbuff[2048],*ppmp;


static int ppmci()
{
	if(ppmin==0)
	{
		ppmin=fread(ppmbuff,sizeof(char),sizeof(ppmbuff),ppmh);
		ppmp=ppmbuff;
	}
	if(ppmin<=0) return -1;
	--ppmin;
	return *ppmp++;
}


static void ppmline(unsigned char *put)
{
	int c;
	while((c=ppmci())>=0)
		if(c==0x0a) break;
		else *put++=c;
	*put=0;
}

/*
 * A PPM file consists of a sequence of one or more PPM images. There are no data, delimiters, or padding before, after, or between images.
 *
 * Each PPM image consists of the following:
 *
 *  1. A "magic number" for identifying the file type. A ppm image's magic
 *     number is the two characters "P6".
 *  2. Whitespace (blanks, TABs, CRs, LFs).
 *  3. A width, formatted as ASCII characters in decimal.
 *  4. Whitespace.
 *  5. A height, again in ASCII decimal.
 *  6. Whitespace.
 *  7. The maximum color value (Maxval), again in ASCII decimal. Must be less
 *     than 65536 and more than zero.
 *  8. A single whitespace character (usually a newline).
 *  9. A raster of Height rows, in order from top to bottom. Each row consists
 *     of Width pixels, in order from left to right. Each pixel is a triplet of
 *     red, green, and blue samples, in that order. Each sample is represented
 *     in pure binary by either 1 or 2 bytes. If the Maxval is less than 256,
 *     it is 1 byte. Otherwise, it is 2 bytes. The most significant byte is first.
 *
 *     A row of an image is horizontal. A column is vertical. The pixels in the
 *     image are square and contiguous.
 *  10. In the raster, the sample values are "nonlinear." They are proportional
 *      to the intensity of the ITU-R Recommendation BT.709 red, green, and blue
 *      in the pixel, adjusted by the BT.709 gamma transfer function. (That
 *      transfer function specifies a gamma number of 2.2 and has a linear
 *      section for small intensities). A value of Maxval for all three samples
 *      represents CIE D65 white and the most intense color in the color universe
 *      of which the image is part (the color universe is all the colors in all
 *      images to which this image might be compared).
 *
 *     ITU-R Recommendation BT.709 is a renaming of the former CCIR Recommendation
 *     709. When CCIR was absorbed into its parent organization, the ITU, ca. 2000,
 *     the standard was renamed. This document once referred to the standard as CIE
 *     Rec. 709, but it isn't clear now that CIE ever sponsored such a standard.
 *
 *     Note that another popular color space is the newer sRGB. A common variation
 *     on PPM is to subsitute this color space for the one specified.
 * 11. Note that a common variation on the PPM format is to have the sample values
 *     be "linear," i.e. as specified above except without the gamma adjustment.
 *     pnmgamma takes such a PPM variant as input and produces a true PPM as output.
 * 12. Strings starting with "#" may be comments, the same as with PBM. 
 */
static int readppm(char *name,struct pic *pic)
{
	unsigned char line[8192];
	int w,h;
	int i,j;
	int maxval;
	unsigned char *put;

	ppmin=0;
	ppmh=fopen(name,"r");
	if(!ppmh)
	{
		fprintf(stderr, "error %d reading \"%s\": %s\n",
			errno, name, strerror(errno));
		return 0;
	}

	/* get magic */
	ppmline(line);
	if(strcmp((char *)line,"P6")) {fclose(ppmh);return 0;}

	/* get width and height */
	ppmline(line);
	if(sscanf((char *)line,"%d %d",&w,&h)!=2) {fclose(ppmh);return 0;}

	/* get max colour value */
	ppmline(line);
	if (sscanf(line, "%d", &maxval) != 1) {fclose(ppmh); return 0; }
	if (maxval > 255)
	{
		fprintf(stderr, "max colour value %d out of range in PPM file \"%s\"\n",
			maxval, name);
	}

	pic->w=w;
	pic->h=h;
	pic->image=put=malloc(w*h*3);
	if(!put) {fclose(ppmh);return 0;}


	/* load raster */
	for(j=0;j<h;++j)
	{
		for(i=0;i<w;++i)
		{
			*put++=ppmci(); /* red */
			*put++=ppmci(); /* green */
			*put++=ppmci(); /* blue */
		}
	}
	return 1;
}

static int loadfont(struct pic *pic)
{
	struct stat sbuf;
	char *fontfile = "data/bigfont.ppm";
	if (stat(fontfile, &sbuf))
	{
		fontfile = DATA_DIR "/bigfont.ppm";
		if (stat(fontfile, &sbuf))
		{
			return 0;
		}
	}
	return readppm(fontfile,pic);
}

void initfont(void)
{
	int i,j,k;
	struct pic pic;
	unsigned char keyr,keyg,keyb,*p;
	unsigned char gapr,gapg,gapb,r,g,b;
	int x,y,xp,yp;
	int cw;
	struct letter *l;
	uint32 *tpixels;
	int fontheight;
	
	init_font_map();
	fontlist = glGenLists(MAXCHARS);

	tpixels=malloc(T_WIDTH*T_HEIGHT*4);
	memset(tpixels,0,T_WIDTH*T_HEIGHT*4);

	if(!loadfont(&pic)) return;

	/* pick out colorkey value from font image */
	p=pic.image+3*(pic.w*pic.h-1);
	keyr=*p++;
	keyg=*p++;
	keyb=*p++;

	p=pic.image+3*pic.w;
	gapr=*p++;
	gapg=*p++;
	gapb=*p++;
	i=pic.w-1;
	k=0;
	j=0;
	l=letters;
	while(i--)
	{
		r=*p++;
		g=*p++;
		b=*p++;
		if(r!=gapr || g!=gapg || b!=gapb) {++j;continue;}
		l++ ->sx=j;
		++k;
		j=0;
	}
	l=letters;
	fontheight=pic.h-2;
	xp=yp=0;
	j=1;
	for(i=0;i<k;++i)
	{
		cw=l->sx;
		if(xp+cw>T_WIDTH)
		{
			xp=0;
			yp+=fontheight;
		}
		l->px=xp;
		l->py=yp;
		l->sy=fontheight;
		++l;
		for(y=0;y<fontheight;++y)
		{
			uint32 *t;

			t=tpixels+(yp+y)*T_WIDTH+xp;
			p=pic.image+3*(pic.w*(y+1)+j);
			for(x=0;x<cw;++x)
			{
				r=*p++;
				g=*p++;
				b=*p++;
				if(r==keyr && g==keyg && b==keyb)
					*t++=0;
				else
					*t++=r | (g<<8) | (b<<16) | (255<<24);
			}
		}
		xp+=cw;
		j+=cw+1;
	}

	glGenTextures( 1, textures );
	glBindTexture( GL_TEXTURE_2D, textures[0] );

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexImage2D( GL_TEXTURE_2D, 0, 4, T_WIDTH, T_HEIGHT, 0, GL_RGBA,
			GL_UNSIGNED_BYTE, tpixels );

	l=letters;
	for(i=0;i<k;++i)
	{
		const float sizex=2.0f*FONTSIZE*l->sx;
		const float sizey=FONTSIZE*l->sy;
		const float sx=l->px/(float)T_WIDTH;
		const float sy=l->py/(float)T_HEIGHT;
		const float ex=(l->px+l->sx-1)/(float)T_WIDTH;
		const float ey=(l->py+l->sy-1)/(float)T_HEIGHT;

		glNewList(fontlist+i, GL_COMPILE);
		{
			/* row = (x, y, u, v) */
			GLfloat v[] = {
				0.0f,  -sizey, sx, ey,
				sizex, -sizey, ex, ey,
				0.0f,   sizey, sx, sy,
				sizex,  sizey, ex, sy
			};

			const GLsizei row_width = (2*2);
			const GLsizei stride = (row_width * sizeof(GLfloat));
			const GLsizei vertex_count = 4;

			glEnableClientState(GL_VERTEX_ARRAY);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			glVertexPointer(2, GL_FLOAT, stride, v);
			glTexCoordPointer(2, GL_FLOAT, stride, v + 2);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, vertex_count);
			glDisableClientState(GL_TEXTURE_COORD_ARRAY);
			glDisableClientState(GL_VERTEX_ARRAY);
		}
		l->advance=FONTSIZE*2+sizex;
		glTranslatef(l->advance, 0.0f, 0.0f);
		glEndList();

		++l;
	}
}


