/**
 * @file matrix.c
 * @brief The implementation of the gljewel matrix module.
 *
 * Copyright 2001, 2008 David Ashley  <dashxdr@gmail.com>
 * Copyright 2008 Stephen M. Webb  <stephen.webb@bregmasoft.ca>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of Version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <math.h>
#include <string.h>
#include <stdio.h>

#include "matrix.h"

#define PI 3.1415926f

matrix identityquick={
{1.0f, 0.0f, 0.0f, 0.0f},
{0.0f, 1.0f, 0.0f, 0.0f},
{0.0f, 0.0f, 1.0f, 0.0f},
{0.0f, 0.0f, 0.0f, 1.0f}
};

matrix zeroquick={
{0.0f, 0.0f, 0.0f, 0.0f},
{0.0f, 0.0f, 0.0f, 0.0f},
{0.0f, 0.0f, 0.0f, 0.0f},
{0.0f, 0.0f, 0.0f, 0.0f}
};

void identitymatrix(matrix *dest)
{
	memcpy(dest,&identityquick,sizeof(matrix));
}
void zeromatrix(matrix *dest)
{
	memcpy(dest,&zeroquick,sizeof(matrix));
}

void matrixXmatrix(matrix *dest,matrix *left,matrix *right)
{
int i,j,k;
matrix temp;
float t;

	for(i=0;i<4;++i)
		for(j=0;j<4;++j)
		{
			t=0;
			for(k=0;k<4;++k)
				t+=(*left)[k][j]*(*right)[i][k];
			temp[i][j]=t;
		}
	memcpy(dest,&temp,sizeof(matrix));
}
void matrixXvector(vector *dest,matrix *left,vector *right)
{
int i,j;
vector temp;
float t;
	for(i=0;i<4;++i)
	{
		t=0;
		for(j=0;j<4;++j)
			t+=(*left)[j][i]*(*right)[j];
		temp[i]=t;
	}
	memcpy(dest,&temp,sizeof(vector));
}
void rotationmatrix(matrix *dest,float angle,float x,float y,float z)
{
float mag,s,c;
float c1,xx,yy,zz,xyc1,xzc1,yzc1,xs,ys,zs;

	mag=sqrtf(x*x+y*y+z*z);
	if(mag==0.0)
	{
		identitymatrix(dest);
		return;
	}
	x/=mag;
	y/=mag;
	z/=mag;
	angle*=PI/180.0f;
	s=sinf(angle);
	c=cosf(angle);
	c1=1.0f-c;
	xx=x*x;
	yy=y*y;
	zz=z*z;
	xyc1=x*y*c1;
	xzc1=x*z*c1;
	yzc1=y*z*c1;
	xs=x*s;
	ys=y*s;
	zs=z*s;

	(*dest)[0][0]=xx*c1+c;
	(*dest)[1][0]=xyc1-zs;
	(*dest)[2][0]=xzc1+ys;
	(*dest)[3][0]=0.0f;
	(*dest)[0][1]=xyc1+zs;
	(*dest)[1][1]=yy*c1+c;
	(*dest)[2][1]=yzc1-xs;
	(*dest)[3][1]=0.0f;
	(*dest)[0][2]=xzc1-ys;
	(*dest)[1][2]=yzc1+xs;
	(*dest)[2][2]=zz*c1+c;
	(*dest)[3][2]=0.0f;
	(*dest)[0][3]=0.0f;
	(*dest)[1][3]=0.0f;
	(*dest)[2][3]=0.0f;
	(*dest)[3][3]=1.0f;
}

void xrotmatrix(matrix *dest,float angle)
{
matrix temp;
float c,s;
	identitymatrix(&temp);
	angle*=PI/180.0f;
	c=cosf(angle);
	s=sinf(angle);
	temp[1][1]=c;
	temp[2][1]=-s;
	temp[1][2]=s;
	temp[2][2]=c;
	matrixXmatrix(dest,&temp,dest);
}
void yrotmatrix(matrix *dest,float angle)
{
matrix temp;
float c,s;
	identitymatrix(&temp);
	angle*=PI/180.0f;
	c=cosf(angle);
	s=sinf(angle);
	temp[0][0]=c;
	temp[2][0]=s;
	temp[0][2]=-s;
	temp[2][2]=c;
	matrixXmatrix(dest,&temp,dest);
}
void zrotmatrix(matrix *dest,float angle)
{
matrix temp;
float c,s;
	identitymatrix(&temp);
	angle*=PI/180.0f;
	c=cosf(angle);
	s=sinf(angle);
	temp[0][0]=c;
	temp[1][0]=-s;
	temp[0][1]=s;
	temp[1][1]=c;
	matrixXmatrix(dest,&temp,dest);
}

float myabs(float x)
{
	return (x<0.0) ? -x : x;
}

void swaprows(matrix *dest,int r1,int r2)
{
int i;
float t;
	for(i=0;i<4;++i)
	{
		t=(*dest)[i][r1];
		(*dest)[i][r1]=(*dest)[i][r2];
		(*dest)[i][r2]=t;
	}
}
void scalerow(matrix *dest,int r1,float scale)
{
int i;
	for(i=0;i<4;++i)
		(*dest)[i][r1]*=scale;
}
void addscalerow(matrix *dest,int r1,int r2,float scale)
{
int i;
	for(i=0;i<4;++i)
		(*dest)[i][r1]+=(*dest)[i][r2]*scale;
}

void printmatrix(matrix *m)
{
int i,j;
	printf("---\n");
	for(j=0;j<4;++j)
	{
		for(i=0;i<4;++i)
			printf(" %7.3f",(*m)[i][j]);
		printf("\n");
	}
}
void invertmatrix(matrix *dest,matrix *source)
{
matrix temp;
int i,k;
float max=0;
int maxnum = 0;
float t;

	identitymatrix(dest);
	memcpy(temp,source,sizeof(temp));
	for(k=0;k<3;++k)
	{
		max=-1;
		for(i=k;i<4;++i)
		{
			t=myabs(temp[k][i]);
			if(t>max)
			{
				max=t;
				maxnum=i;
			}
		}
		if(maxnum!=k)
		{
			swaprows(dest,k,maxnum);
			swaprows(&temp,k,maxnum);
		}
		t=1.0f/temp[k][k];
		scalerow(dest,k,t);
		scalerow(&temp,k,t);
		for(i=k+1;i<4;++i)
		{
			t=-temp[k][i];
			if(t!=0.0f)
			{
				addscalerow(dest,i,k,t);
				addscalerow(&temp,i,k,t);
			}
		}
	}
	for(k=3;k>0;--k)
	{
		for(i=0;i<k;++i)
		{
			t=-temp[k][i];
			if(t!=0.0f)
			{
				addscalerow(dest,i,k,t);
				addscalerow(&temp,i,k,t);
			}
		}
	}
}
