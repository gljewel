/**
 * @file pyramid.c
 * @brief The pyramid module.
 *
 * Copyright 2001, 2008 David Ashley  <dashxdr@gmail.com>
 * Copyright 2008 Stephen M. Webb  <stephen.webb@bregmasoft.ca>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of Version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "pyramid.h"

#if defined(_WIN32) && !defined(__CYGWIN__)
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif

#include <GL/gl.h>
#include "vbo.h"

static const int row_width = (3*2);
static const int bottom_vertex_count = 4;
static const int side_vertex_count = 6;
#define stride (row_width * sizeof(GLfloat))

static GLuint vbo;


static void create(float size)
{
	GLfloat pyramid[] = {
		 /* bottom */
		 0.0f, -1.0f,  0.0f,  size, -size,  size,
		 0.0f, -1.0f,  0.0f,  size, -size, -size, 
		 0.0f, -1.0f,  0.0f, -size, -size, -size,
		 0.0f, -1.0f,  0.0f, -size, -size,  size,
		 /* sides */
		 0.0f,  1.0f,  0.0f,  0.0f,  size,  0.0f,
		 1.0f, -1.0f,  1.0f,  size, -size,  size,
		 1.0f, -1.0f, -1.0f,  size, -size, -size,
		-1.0f, -1.0f, -1.0f, -size, -size, -size,
		-1.0f, -1.0f,  1.0f, -size, -size,  size,
		 1.0f, -1.0f,  1.0f,  size, -size,  size
	};
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramid), pyramid, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}


static void draw(void)
{
	GLfloat *pyramid_normals = 0;
	GLfloat *pyramid_verteces = pyramid_normals + 3;
	glEnable(GL_NORMALIZE);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glNormalPointer(GL_FLOAT, stride, pyramid_normals);
	glVertexPointer(3, GL_FLOAT, stride, pyramid_verteces);
	glDrawArrays(GL_TRIANGLE_FAN, 0, bottom_vertex_count);
	glDrawArrays(GL_TRIANGLE_FAN, bottom_vertex_count, side_vertex_count);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisable(GL_NORMALIZE);
}


static char* name(void)
{
	return "pyramid";
}


void create_pyramid(shape* shape)
{
	shape->create = create;
	shape->draw = draw;
	shape->name = name;
}


void makepyramid(float size)
{
	GLfloat pyramid[] = {
		 /* bottom */
		 0.0f, -1.0f,  0.0f,  size, -size,  size,
		 0.0f, -1.0f,  0.0f,  size, -size, -size, 
		 0.0f, -1.0f,  0.0f, -size, -size, -size,
		 0.0f, -1.0f,  0.0f, -size, -size,  size,
		 /* sides */
		 0.0f,  1.0f,  0.0f,  0.0f,  size,  0.0f,
		 1.0f, -1.0f,  1.0f,  size, -size,  size,
		 1.0f, -1.0f, -1.0f,  size, -size, -size,
		-1.0f, -1.0f, -1.0f, -size, -size, -size,
		-1.0f, -1.0f,  1.0f, -size, -size,  size,
		 1.0f, -1.0f,  1.0f,  size, -size,  size
	};

	const GLfloat *pyramid_normals = pyramid;
	const GLfloat *pyramid_verteces = pyramid_normals + 3;

	glEnable(GL_NORMALIZE);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);

	glNormalPointer(GL_FLOAT, stride, pyramid_normals);
	glVertexPointer(3, GL_FLOAT, stride, pyramid_verteces);
	glDrawArrays(GL_TRIANGLE_FAN, 0, bottom_vertex_count);
	glDrawArrays(GL_TRIANGLE_FAN, bottom_vertex_count, side_vertex_count);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}


