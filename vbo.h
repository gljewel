/**
 * @file vbo.h
 * @brief A wrapper around the Vertex Buffer extension to OpenGL 1.1.
 *
 * Copyright 2008 Stephen M. Webb  <stephen.webb@bregmasoft.ca>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of Version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef GLJEWEL_VBO_H_
#define GLJEWEL_VBO_H_

#if defined(_WIN32) && !defined(__CYGWIN__)
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif
#include <GL/gl.h>
#ifdef HAVE_GLEXT_H
# define GL_GLEXT_PROTOTYPES
# include <GL/glext.h>
#endif

#ifndef HAVE_GL_VERTEX_BUFFERS
# ifndef GL_ARRAY_BUFFER
#  define GL_ARRAY_BUFFER 0x8892
# endif
# ifndef GL_STATIC_DRAW
#  define GL_STATIC_DRAW 0x88E4
# endif

typedef void (APIENTRY *PFNGLBINDBUFFERPROC)(GLenum, GLuint);
typedef void (APIENTRY *PFNGLDELETEBUFFERSPROC)(GLsizei, const GLuint*);
typedef void (APIENTRY *PFNGLGENBUFFERSPROC)(GLsizei, GLuint*);
typedef void (APIENTRY *PFNGLBUFFERDATAPROC)(GLenum, int, const GLvoid*, GLenum);

PFNGLBINDBUFFERPROC    glBindBuffer;
PFNGLDELETEBUFFERSPROC glDeleteBuffers;
PFNGLGENBUFFERSPROC    glGenBuffers;
PFNGLBUFFERDATAPROC    glBufferData;
#endif

void initGlVboExtension(void);

#endif /* GLJEWEL_VBO_H_ */
