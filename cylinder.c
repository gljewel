/**
 * @file cylinder.c
 * @brief The celerey module.
 *
 * Copyright 2001, 2008 David Ashley  <dashxdr@gmail.com>
 * Copyright 2008 Stephen M. Webb  <stephen.webb@bregmasoft.ca>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of Version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "cylinder.h"

#if defined(_WIN32) && !defined(__CYGWIN__)
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif

#include <GL/gl.h>
#include <math.h>
#include "misc.h"


/** Celery stalks */
#define CSQUEEZE 0.8f
void makecylinder(float size)
{
#define CSIDES 12
float x[CSIDES],z[CSIDES];
int i,j,k;
float a;
	for(i=0;i<CSIDES;++i)
	{
		a=i*PI*2.0f/CSIDES;
		x[i]=cosf(a)*size*CSQUEEZE;
		z[i]=sinf(a)*size*CSQUEEZE;
	}

	glEnable(GL_NORMALIZE);
	for(j=0;j<2;++j)
	{
		float p=(j&1) ? -size : size;
		glNormal3f(0.0, p, 0.0);
		glBegin(GL_POLYGON);
		for(i=0;i<CSIDES;++i)
		{
			k=(j&1) ? i : (CSIDES-1-i);
			glVertex3f(x[k],p,z[k]);
		}
		glEnd();
	}
	glBegin(GL_QUAD_STRIP);
	for(i=0;i<CSIDES+1;++i)
	{
		j=(i<CSIDES) ? i : i-CSIDES;
		if(i)
			glNormal3f((x[j]+x[k])/2.0f,0.0f,(z[j]+z[k])/2.0f);
		glVertex3f(x[j],-size,z[j]);
		glVertex3f(x[j], size,z[j]);
		k=j;
	}
	glEnd();

}



