/**
 * @file sphere.c
 * @brief The snowball module.
 *
 * Copyright 2001, 2008 David Ashley  <dashxdr@gmail.com>
 * Copyright 2008 Stephen M. Webb  <stephen.webb@bregmasoft.ca>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of Version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "sphere.h"

#if defined(_WIN32) && !defined(__CYGWIN__)
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif

#include <GL/gl.h>
#include <math.h>
#include "misc.h"


void makeuvsphere(float size)
{
#define USIDES 15
#define VSIDES 9
float x[USIDES],z[USIDES];
int i,j,t,o;
float a;
float c1,s1,c2,s2;
	for(i=0;i<USIDES;++i)
	{
		a=i*PI*2.0f/USIDES;
		x[i]=cosf(a)*size;
		z[i]=sinf(a)*size;
	}

	glEnable(GL_NORMALIZE);

	for(i=0;i<VSIDES;++i)
	{
		a=i*PI/VSIDES;
		c1=cosf(a);
		s1=sinf(a);
		a=(i+1)*PI/VSIDES;
		c2=cosf(a);
		s2=sinf(a);
		glBegin(GL_QUAD_STRIP);
		for(j=0;j<USIDES+1;++j)
		{
			t=(j<USIDES) ? j : j-USIDES;
			if(j)
			{
				float s,c;
				a=(i+0.5f)*PI/VSIDES;
				c=cosf(a);
				s=sinf(a);
				a=(j-0.5f)*PI*2.0f/USIDES;
				glNormal3f(cosf(a)*s,c,sinf(a)*s);
			}
			glVertex3f(x[t]*s2,c2*size,z[t]*s2);
			glVertex3f(x[t]*s1,c1*size,z[t]*s1);
			o=t;
		}
		glEnd();
	}
}


