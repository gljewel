/**
 * @file ico.c
 * @brief The icosahedron module.
 *
 * Copyright 2001, 2008 David Ashley  <dashxdr@gmail.com>
 * Copyright 2008 Stephen M. Webb  <stephen.webb@bregmasoft.ca>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of Version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "ico.h"

#if defined(_WIN32) && !defined(__CYGWIN__)
# define WIN32_LEAN_AND_MEAN 1
# include <windows.h>
#endif

#include <GL/gl.h>
#include <math.h>
#include "misc.h"
#include <string.h>

#define X .525731112119133606f
#define Z .850650808352039932f

static GLfloat vdata [12][3] = {
{-X, 0.0, Z}, {X, 0.0, Z}, {-X, 0.0, -Z},
{X, 0.0, -Z}, {0.0, Z, X}, {0.0, Z, -X},
{0.0, -Z, X}, {0.0, -Z, -X}, {Z, X, 0.0},
{-Z, X, 0.0}, {Z, -X, 0.0}, {-Z, -X, 0.0}
};

static GLuint tindices [20][3] = {
 {1, 4, 0}, {4, 9, 0},
 {4, 5, 9}, {8, 5, 4},
 {1, 8, 4}, {1, 10, 8},
 {10, 3, 8}, {8, 3, 5},
 {3, 2, 5}, {3, 7, 2},
 {3, 10, 7}, {10, 6, 7},
 {6, 11, 7}, {6, 0, 11},
 {6, 1, 0}, {10, 1, 6},
 {11, 0, 9}, {2, 11, 9},
 {5, 2, 9}, {11, 2, 7}
};

void dup3(float *a,float *b)
{
	*a++=*b++;
	*a++=*b++;
	*a=*b;
}

void mid(float *a,float *b,float *c)
{
float d;
	a[0]=(*b++ + *c++)/2.0f;
	a[1]=(*b++ + *c++)/2.0f;
	a[2]=(*b + *c)/2.0f;
	d=1.0f/sqrtf(a[0]*a[0]+a[1]*a[1]+a[2]*a[2]);
	a[0]*=d;
	a[1]*=d;
	a[2]*=d;

}


void makeicosahedron(int sub,float scale)
{
float tris[65536][3];
int i,j,p,q;
int numt;
	numt=0;
	j=0;
	for(i=0;i<20;++i)
	{
		p=tindices[i][0];
		tris[j][0]=vdata[p][0];
		tris[j][1]=vdata[p][1];
		tris[j][2]=vdata[p][2];
		++j;
		p=tindices[i][1];
		tris[j][0]=vdata[p][0];
		tris[j][1]=vdata[p][1];
		tris[j][2]=vdata[p][2];
		++j;
		p=tindices[i][2];
		tris[j][0]=vdata[p][0];
		tris[j][1]=vdata[p][1];
		tris[j][2]=vdata[p][2];
		++j;
		++numt;
	}
	while(sub--)
	{
		j=numt;
		numt<<=2;
		while(--j>=0)
		{
			float ttt[3][3];
			p=j*3;
			q=p*4;
			memcpy(ttt,tris[p],sizeof(ttt));

			mid(tris[q++],ttt[0],ttt[1]);
			dup3(tris[q++],ttt[1]);
			mid(tris[q++],ttt[1],ttt[2]);

			dup3(tris[q++],ttt[0]);
			mid(tris[q++],ttt[0],ttt[1]);
			mid(tris[q++],ttt[0],ttt[2]);

			mid(tris[q++],ttt[1],ttt[2]);
			dup3(tris[q++],ttt[2]);
			mid(tris[q++],ttt[0],ttt[2]);

			mid(tris[q++],ttt[0],ttt[1]);
			mid(tris[q++],ttt[1],ttt[2]);
			mid(tris[q++],ttt[0],ttt[2]);

		}
	}
	glEnable(GL_NORMALIZE);
	p=0;
	glBegin(GL_TRIANGLES);
	for(i=0;i<numt;++i)
	{
		norm2(tris[p],tris[p+1],tris[p+2]);
		glVertex3f(tris[p][0]*scale,tris[p][1]*scale,tris[p][2]*scale);
		++p;
		glVertex3f(tris[p][0]*scale,tris[p][1]*scale,tris[p][2]*scale);
		++p;
		glVertex3f(tris[p][0]*scale,tris[p][1]*scale,tris[p][2]*scale);
		++p;
	}
	glEnd();
}

